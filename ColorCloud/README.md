# ColorCloud - Generator of `.xyz` files

## Prerequisites

Install the following packages:

```sh
# OpenGL libraries: mesa-utils
# GLUT: freeglut3-dev
# GLM: libglm-dev

sudo apt-get install libxmu-dev libxi-dev \
                mesa-utils \
                freeglut3-dev \
                libglm-dev
```

## Generate makefile by CMake

```sh
cmake .
```

## Compile

```sh
make
```

## Run

```sh
./ColorCloud
```
